"use strict";

/**
 *
 *	Plugin initializer
 *
 */

/* global Plugins: true */

const fs = require('fs');
const path = require('path');
const cluster = require('cluster');

const yamljs = require('yamljs');

const object_merge = require('./utils/object-merge');
const ErrorCreator = require('./plugin-error');

let Plugins = require('./');
const LoaderError = new ErrorCreator('@@Loader');
let eventEmitter = null;
let plugins = {};

const errorPreHandler = function (error) {
	if (!(error instanceof Error)) throw new TypeError(`Did not emit a proper Error instance`);
	const severity = error.severity || 'critical';
	this.env('severity', severity);

	const severityTable = ['debug', 'warning', 'minor', 'critical', 'fatal'];
	if (severityTable.indexOf(severity) < 0) {
		console.error(`Error emitted with invalid severity: "${severity}".`);
		this.env('severity', 'fatal'); // RIP
	}
};
const errorPostHandler = function (error, options = {}) {
	if (!error || !options || typeof options !== 'object') throw new Error(`Bad error event parameters`);
	if (!this.isDefaultPrevented()) {
		if (error.source && !(error instanceof LoaderError)) Plugins.addErrorLogger(error.source)("" + error.stack);
		const severity = this.env('severity');
		if (severity === 'fatal') throw error; // bubble-up
		const severityTable = ['debug', 'warning', 'minor', 'critical', 'fatal'];
		if (severityTable.indexOf(severity) >= severityTable.indexOf(Plugins.config.logLevel || 'critical')) {
			const fakeErr = Object.create(Object.getPrototypeOf(error));
			Object.getOwnPropertyNames(error).forEach(function (name) {
				fakeErr[name] = error[name];
			});
			if (error.originalError) options.trueStack = error.originalError.stack;
			require('./../crashlogger')(fakeErr, error.source ? `Plugin ${error.source}` : `A plugin`, options);
		}
	}
};

/**
 * Commands useful for any plugin
 *	The /setlanguage command is currently needed for a server-side multilanguage implementation of tour.js
 *	However, it's expected to be useful for any other plugin supporting more than one language.
 *
 *	The behavior of hotpatch described above is only true if the /hotpatch command implemented here is used.
 *
 *	The command /eval (and, similarly, its shortcut >>) will now support /eval plugin:<name> <code> to call the 'eval' method of a plugin.
 *	The original syntax will still be available. However, the original command /eval in 'chat-commands.js' must be renamed to /evalcommands
 *	in order to keep the context of 'chat-commands.js' available to the hereby provided /eval command.
 *
 */
const pluginCommands = {
	language: 'setlanguage',
	setlanguage(target, room, user) {
		if (!this.can('makeroom')) return;
		const targetId = toId(target);
		const validLanguage = {'spanish':1, 'english':1, 'french':1, 'portuguese':1, 'italian':1, 'multilanguage':1};
		if (!(targetId in validLanguage)) return this.sendReply("" + target + " es un idioma no valido por ahora. Son validos: " + Object.keys(validLanguage).join(", "));
		room.language = targetId;
		this.sendReply("El idioma de la sala ha sido cambiado a " + target);
		if (room.chatRoomData) {
			room.chatRoomData.language = targetId;
			Rooms.global.writeChatRoomData();
		}
	},
	forcehotpatch: 'hotpatch',
	async hotpatch(target, room, user, connection, cmd) {
		if (!target) return this.parse('/help hotpatch');
		if (!this.can('hotpatch')) return;

		if (Monitor.updateServerLock) {
			return this.errorReply("Wait for /updateserver to finish before hotpatching.");
		}
		const lock = Monitor.hotpatchLock;
		const hotpatches = ['chat', 'formats', 'loginserver', 'punishments', 'dnsbl'];
		const version = await Monitor.version();
		const requiresForce = (patch) =>
			version && cmd !== 'forcehotpatch' &&
			(Monitor.hotpatchVersions[patch] ?
				Monitor.hotpatchVersions[patch] === version :
				(global.__version && version === global.__version.tree));
		const requiresForceMessage = `The git work tree has not changed since the last time ${target} was hotpatched (${version.slice(0, 8)}), use /forcehotpatch ${target} if you wish to hotpatch anyway.`;

		this.logEntry("" + user.name + " utilizó /hotpatch " + target);
		let patch = target;

		try {
			if (target === 'all') {
				if (lock['all']) {
					return this.errorReply(`Hot-patching all has been disabled by ${lock['all'].by} (${lock['all'].reason})`);
				}
				if (Config.disablehotpatchall) {
					return this.errorReply("This server does not allow for the use of /hotpatch all");
				}

				for (const hotpatch of hotpatches) {
					this.parse(`/hotpatch ${hotpatch}`);
				}
			} else if (target === 'chat' || target === 'commands') {
				patch = 'chat';
				if (lock['chat']) {
					return this.errorReply(`Hot-patching chat has been disabled by ${lock['chat'].by} (${lock['chat'].reason})`);
				}
				if (lock['tournaments']) {
					return this.errorReply(`Hot-patching tournaments has been disabled by ${lock['tournaments'].by} (${lock['tournaments'].reason})`);
				}
				if (requiresForce(patch)) return this.errorReply(requiresForceMessage);

				global.Chat = require('./../server/chat').Chat;
				Chat.uncacheDir('./server/chat');
				Chat.uncacheDir('./translations');
				Chat.loadPlugins();

						/**
				 * Plugin hotpatch
				 */

				const pluginCache = {
					dynamic: {},
					dataLoaded: {},
					version: {},
				};

				Plugins.forEach(function (plugin) {
					if (!plugin || typeof plugin !== 'object' && typeof plugin !== 'function') return;

					const id = plugin.id;
					if (typeof plugin.deinit === 'function') {
						plugin.deinit();
					}
					if (plugin === global[plugin.globalScope]) {
						delete global[plugin.globalScope];
					}
					if (typeof plugin.dynamic === 'object') {
						pluginCache.dynamic[id] = plugin.dynamic;
					}
					pluginCache.dataLoaded[id] = !!plugin.dataLoaded;
					pluginCache.version[id] = plugin.version;
				});

				global.Plugins = Chat.uncacheDir('./plugins');
				Plugins = global.Plugins; // make sure to override the local variable as well
				eventEmitter = Plugins.eventEmitter;
				plugins = Plugins.init().plugins;
				eventEmitter.setMaxListeners(Object.keys(plugins).length);

				Plugins.forEach(function (plugin) {
					if (!plugin || typeof plugin !== 'object' && typeof plugin !== 'function') {
						eventEmitter.emit('error', new Error("Plugin inválido.")).flush();
					}
					const id = plugin.id;
					if (plugin.commands && typeof plugin.commands === 'object') {
						object_merge(Chat.commands, plugin.commands, {deep: true});
					}
					if (typeof plugin.init === 'function') {
						plugin.init(pluginCache.version[id], pluginCache.dynamic[id]);
					}

					if (pluginCache.dynamic[id]) {
						object_merge(plugin.dynamic, pluginCache.dynamic[id], {deep: true});
					}

					if (typeof plugin.loadData === 'function' && !pluginCache.dataLoaded[id]) {
						plugin.loadData(pluginCache.version[id]);
					} else {
						plugin.dataLoaded = pluginCache.dataLoaded[id];
					}
					if (typeof plugin.onLoad === 'function') {
						plugin.onLoad();
					}				
					if (typeof plugin.onPlugin === 'function') {
						plugin.onPlugin(true);
					}
					if (plugin.globalScope) {
						global[typeof plugin.globalScope === 'string' ? plugin.globalScope : id] = plugin;
					}
				});

				Chat.uncacheDir('./server/tournaments');
				global.Tournaments = require('../server/tournaments').Tournaments;
				Chat.loadPluginData(Tournaments);

				this.sendReply("Los plugins han sido actualizados.");

			} else if (target === 'tournaments') {
				if (lock['tournaments']) {
					return this.errorReply(`Hot-patching tournaments has been disabled by ${lock['tournaments'].by} (${lock['tournaments'].reason})`);
				}
				if (requiresForce(patch)) return this.errorReply(requiresForceMessage);

				Chat.uncacheDir('./server/tournaments');
				global.Tournaments = require('../tournaments').Tournaments;
				Chat.loadPluginData(Tournaments);
				this.sendReply("Tournaments have been hot-patched.");
			} else if (target === 'formats' || target === 'battles') {
				patch = 'formats';
				if (lock['formats']) {
					return this.errorReply(`Hot-patching formats has been disabled by ${lock['formats'].by} (${lock['formats'].reason})`);
				}
				if (lock['battles']) {
					return this.errorReply(`Hot-patching battles has been disabled by ${lock['battles'].by} (${lock['battles'].reason})`);
				}
				if (lock['validator']) {
					return this.errorReply(`Hot-patching the validator has been disabled by ${lock['validator'].by} (${lock['validator'].reason})`);
				}
				if (requiresForce(patch)) return this.errorReply(requiresForceMessage);

				// uncache the .sim-dist/dex.js dependency tree
				Chat.uncacheDir('./.sim-dist');
				Chat.uncacheDir('./.data-dist');
				Chat.uncache('./config/formats');
				// reload .sim-dist/dex.js
				global.Dex = require('../sim/dex').Dex;
				// rebuild the formats list
				delete Rooms.global.formatList;
				// respawn validator processes
				void TeamValidatorAsync.PM.respawn();
				// respawn simulator processes
				void Rooms.PM.respawn();
				// broadcast the new formats list to clients
				Rooms.global.send(Rooms.global.formatListText);

				this.sendReply("Formats have been hot-patched.");
			} else if (target === 'loginserver') {
				if (requiresForce(patch)) return this.errorReply(requiresForceMessage);
				FS('config/custom.css').unwatch();
				Chat.uncache('./.server-dist/loginserver');
				global.LoginServer = require('../server/loginserver').LoginServer;
				this.sendReply("The login server has been hot-patched. New login server requests will use the new code.");
			} else if (target === 'learnsets' || target === 'validator') {
				patch = 'validator';
				if (lock['validator']) {
					return this.errorReply(`Hot-patching the validator has been disabled by ${lock['validator'].by} (${lock['validator'].reason})`);
				}
				if (lock['formats']) {
					return this.errorReply(`Hot-patching formats has been disabled by ${lock['formats'].by} (${lock['formats'].reason})`);
				}
				if (requiresForce(patch)) return this.errorReply(requiresForceMessage);

				void TeamValidatorAsync.PM.respawn();
				this.sendReply("The team validator has been hot-patched. Any battles started after now will have teams be validated according to the new code.");
			} else if (target === 'punishments') {
				patch = 'punishments';
				if (lock['punishments']) {
					return this.errorReply(`Hot-patching punishments has been disabled by ${lock['punishments'].by} (${lock['punishments'].reason})`);
				}
				if (requiresForce(patch)) return this.errorReply(requiresForceMessage);

				Chat.uncache('./.server-dist/punishments');
				global.Punishments = require('../server/punishments').Punishments;
				this.sendReply("Punishments have been hot-patched.");
			} else if (target === 'dnsbl' || target === 'datacenters' || target === 'iptools') {
				patch = 'dnsbl';
				if (requiresForce(patch)) return this.errorReply(requiresForceMessage);

				Chat.uncache('./.server-dist/ip-tools');
				global.IPTools = require('../server/ip-tools').IPTools;
				void IPTools.loadDatacenters();
				this.sendReply("IPTools has been hot-patched.");
			} else if (target.startsWith('disable')) {
				this.sendReply("Disabling hot-patch has been moved to its own command:");
				return this.parse('/help nohotpatch');
			} else {
				return this.errorReply("Your hot-patch command was unrecognized.");
			}
		} catch (e) {
			Rooms.global.notifyRooms(
				['development', 'staff', 'upperstaff'],
				`|c|${user.getIdentity()}|/log ${user.name} used /hotpatch ${patch} - but something failed while trying to hot-patch.`
			);
			return this.errorReply(`Something failed while trying to hot-patch ${patch}: \n${e.stack}`);
		}
		Monitor.hotpatchVersions[patch] = version;
		Rooms.global.notifyRooms(
			['development', 'staff', 'upperstaff'],`|c|${user.getIdentity()}|/log ${user.name} utilizo /hotpatch ${patch}`
		);
	},
	hotpatchhelp: [
		`Hot-patching the game engine allows you to update parts of Showdown without interrupting currently-running battles. Requires: ~`,
		`Hot-patching has greater memory requirements than restarting`,
		`You can disable various hot-patches with /nohotpatch. For more information on this, see /help nohotpatch`,
		`/hotpatch chat - reload chat-commands.js and the chat-plugins`,
		`/hotpatch validator - spawn new team validator processes`,
		`/hotpatch formats - reload the .sim-dist/dex.js tree, rebuild and rebroad the formats list, and spawn new simulator and team validator processes`,
		`/hotpatch dnsbl - reloads IPTools datacenters`,
		`/hotpatch punishments - reloads new punishments code`,
		`/hotpatch loginserver - reloads new loginserver code`,
		`/hotpatch tournaments - reloads new tournaments code`,
		`/hotpatch all - hot-patches chat, tournaments, formats, login server, punishments, and dnsbl`,
		`/forcehotpatch [target] - as above, but performs the update regardless of whether the history has changed in git`,
	],

	hotpatchlock: 'nohotpatch',
	nohotpatch(target, room, user) {
		if (!this.can('declare')) return;
		if (!target) return this.parse('/help nohotpatch');

		const separator = ' ';

		const hotpatch = toID(target.substr(0, target.indexOf(separator)));
		const reason = target.substr(target.indexOf(separator), target.length).trim();
		if (!reason || !target.includes(separator)) return this.parse('/help nohotpatch');

		const lock = Monitor.hotpatchLock;
		const validDisable = ['chat', 'battles', 'formats', 'validator', 'tournaments', 'punishments', 'all'];

		if (validDisable.includes(hotpatch)) {
			if (lock[hotpatch]) {
				return this.errorReply(`Hot-patching ${hotpatch} has already been disabled by ${lock[hotpatch].by} (${lock[hotpatch].reason})`);
			}
			lock[hotpatch] = {
				by: user.name,
				reason,
			};
			this.sendReply(`You have disabled hot-patching ${hotpatch}.`);
		} else {
			return this.errorReply("This hot-patch is not an option to disable.");
		}
		Rooms.global.notifyRooms(
			['development', 'staff', 'upperstaff'],
			`|c|${user.getIdentity()}|/log ${user.name} has disabled hot-patching ${hotpatch}. Reason: ${reason}`
		);
	},
	nohotpatchhelp: [
		`/nohotpatch [chat|formats|battles|validator|tournaments|punishments|all] [reason] - Disables hotpatching the specified part of the simulator. Requires: & ~`,
	],
	
	'eval': function (target, room, user, connection, cmd) {
		/* eslint-disable no-unused-vars */
		if (!user.hasConsoleAccess(connection)) {
			return this.sendReply("" + this.cmdToken + cmd + " - Acceso denegado.");
		}
		if (!this.runBroadcast()) return;

		Plugins.eventEmitter.emit('console', cmd, target, connection).flush();

		try {
			const battle = room.battle;
			const me = user;
			let name;
			let plugin;
			let method;
			if (target.startsWith('plugin:')) {
				target = target.slice(7);
				let spaceIndex = -1;
				let startPos = 0;
				let substring = target;
				let escapeCharCount = 0;
				while (spaceIndex === -1) {
					spaceIndex = target.indexOf(' ', startPos);
					if (spaceIndex === -1) {
						name = target.replace(/\\\\/g, '\\');
						target = '';
						break;
					}
					substring = target.substr(0, spaceIndex);
					escapeCharCount = 0;
					for (let i = substring.length - 1; i > 0; i--) {
						if (substring[i] !== '\\') break;
						escapeCharCount++;
					}
					if (escapeCharCount % 2 === 1) {
						// the found space character was actually escaped
						substring = substring.replace('\\ ', ' ');
						target = target.replace('\\ ', ' ');
						startPos = spaceIndex;
						spaceIndex = -1;
					} else {
						name = substring.replace(/\\\\/g, '\\');
						target = target.slice(spaceIndex + 1);
					}
				}
				plugin = plugins[name];
				if (!plugin) throw new Error("No plugin was found to match the name '" + name + "'.");
				method = plugin.eval;
				if (!method) throw new Error("Plugin '" + name + "' has no method 'eval'.");
			}
			if (method) {
				if (!this.broadcasting) this.sendReply('||>> ' + target);
				this.sendReply('||<< ' + Plugins.inspect(method.call(this, target, room, user, connection)));
			} else if (Chat.commands['evalcommands']) {
				this.target = target;
				this.run('evalcommands');
			} else {
				if (!this.runBroadcast()) return;
				if (!this.broadcasting) this.sendReply('||>> ' + target);
				this.sendReply('||<< ' + Plugins.inspect(eval(target, room, user, connection)));
			}
		} catch (e) {
			this.sendReply('||<< error: ' + e.message);
			const stack = '||' + ('' + e.stack).replace(/\n/g, '\n||');
			connection.sendTo(room, stack);
		}
		/* eslint-enable no-unused-vars */
	},
};

/**
 *
 *  Event system applied
 *  Pokémon Showdown's internal methods, adapted to work with plugins.
 *
 */

 /**
 *	GlobalRoom methods
 *	In all the cases, the caller's arguments are passed to the event.
 *  Some other arguments may be added, depending on the case. See the specific method for details.
 *
 *		'startBattle' emits the 'BattleStart' event.
 *			In addition to all the native arguments of `startBattle`, the new room is passed to the event.
 *			If any call of 'onStartBattle' returns exactly null, then the default message for battle start will not be shown regardless of ´Config.reportbattles´.
 */

exports = module.exports = function () {
	eventEmitter = Plugins.eventEmitter;

	// Initialize error handling for Plugin event emitter
	eventEmitter.on('error', errorPreHandler);
	if (Plugins.config.errorHandler) {
		if (typeof Plugins.config.errorHandler !== 'function') {
			throw new TypeError(
				"Error en configuración de plugins: " +
				"el manipulador de excepciones debe ser una función, " +
				"no un " + typeof Plugins.config.errorHandler + "."
			);
		}
		eventEmitter.on('error', Plugins.config.errorHandler);
	}
	eventEmitter.on('error', errorPostHandler);

	// Create our needed folders in the file system
	try {
		fs.mkdirSync(__dirname + '/logs');
	} catch (err) {
		if (err.code !== 'EEXIST') {
			eventEmitter.emit('error', err);
			if (!eventEmitter.isDefaultPrevented()) {
				Plugins.log = function () {};
			}
			eventEmitter.flush();
		}
	}
	try {
		fs.mkdirSync(__dirname + '/logs/errors');
	} catch (err) {
		if (err.code !== 'EEXIST') {
			eventEmitter.emit('error', err);
			if (!eventEmitter.isDefaultPrevented()) {
				Plugins.log = function () {};
			}
			eventEmitter.flush();
		}
	}

	if (cluster.isMaster) {
		// Implement commands
		Chat.loadPlugins(); // Ty main
		Object.assign(Chat.commands, pluginCommands);
	}

	/**
	 *	Time to actually load the plugins
	 */

	try {
		const pluginList = yamljs.load(path.join(__dirname, 'index.yaml'));
		if (!Array.isArray(pluginList)) throw new SyntaxError();
		for (let i = 0, len = pluginList.length; i < len; i++) {
			let pluginPath = pluginList[i];
			if (!Plugins.validateName(pluginPath)) throw new SyntaxError("Nombre de plugin inválido");
			if (plugins[pluginPath]) continue;
			try {
				Plugins.load(pluginPath);
				if (Object.prototype.hasOwnProperty.call(Plugins.plugins, pluginPath)) {
					Object.defineProperty(plugins, pluginPath, Object.getOwnPropertyDescriptor(Plugins.plugins, pluginPath));
				}
			} catch (e) {
				e.source = '@@Loader';
				eventEmitter.emit('error', new LoaderError("Error al cargar plugin `" + pluginPath + "`", e)).flush();
			}
		}
	} catch (err) {
		if (!err.stack && err.snippet) err.stack = "Error al interpretar L" + (err.parsedLine || "?") + ": '" + err.snippet + "'";
		eventEmitter.emit('error', new LoaderError("Formato de lista de plugins inválido", err)).flush();
	}

	if (Config.isInitialization) Plugins.initData();

	return exports;
};
